#!/bin/sh
#
#SBATCH --partition=shared
#SBATCH --time=0:5:0
#SBATCH --job-name=test
#SBATCH --cpus-per-task=1
#SBATCH --tasks-per-node=1
#SBATCH --ntasks=1
##SBATCH --mail-user=sierakowski@jhu.edu
##SBATCH --mail-type=END

# load modules


pwd
echo $SLURM_ARRAY_TASK_ID
dir=experiment/ex_$SLURM_ARRAY_TASK_ID
mkdir $dir
cd $dir
touch "i_was_here_$SLURM_ARRAY_TASK_ID"

pwd
sleep 10
echo Hi

mv ../../slurm-$SLURM_ARRAY_JOB_ID\_$SLURM_ARRAY_TASK_ID\.out .
