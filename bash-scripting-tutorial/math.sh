#!/bin/bash

# DON'T DO IT! (integer math is okay...)

array=(a b c d)
for i in {0..2}
do
  echo ${array[$i]}
done

a=31/2
echo $a

let a=31/2
echo $a

let b=10+20
echo $b

c=$((20+30))
echo $c

d=$(($b*$c))
echo $d

c="hello"
d="hello"

if [ $a -eq $(($b-$a)) ] # -gt -lt
then
  echo equal
else
  echo not equal
fi

if [ $c == $d ]
then
  echo equal
else
  echo not equal
fi

function compare () {
  if [ $1 -eq $2 ]
  then
    echo $1 is equal to $2
  else
    echo $1 is not equal to $2
  fi
}

compare $a 15
