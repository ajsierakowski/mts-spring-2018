#!/bin/bash

a=Hello
b=World

echo $a $b

dir=/home-4/t-asierak1@jhu.edu

ls $dir

# single quotes are literal
c='Hello World'
echo $c

# double quotes substitute
d="$a ... $b!"
echo $d

# store results of command
e=$(ls $dir | wc -l)
echo $e
