#!/usr/bin/env python

a = 1
a = 'hi mom'
print(a)

b = 123 + 456
print(b)
c = 1.5 * 4
print(c)
d = 2 ** 8
print(d)

H = 'Hopkins'
print(H)
print(len(H))
print(H[0])
print(H[6])
print(H[-1])
print(H[0:3])
print(H[3:])
print(H[:5])
print(H + ' University')
HOP = H + ' University'
print(HOP)
print('I am at %s University' % (H,))
print(H, 'University')
print(H + ' University')
print(H * 4)
print(H.find('pki'))
print(H.find('pi'))
print(H.replace('op', 'ad'))
#H = H.replace('op', 'ad')
print(H)

# booleans
T = True
F = False
N = None

# lists
L = ['JHU', 1876, None]
print(L)
print(L[2] == T)
L[2] = T
print(L)
#H[2] = 'a' can't do this
L.append('Baltimore')
print(L)
L.pop(2)
print(L)
print(len(L))
#print(L.sort()) can't do this
L[1] = 'Homewood'
L.sort()
print(L)

# dictionaries
D = {'school': 'Hopkins', 'nstudents': 12345}
print(D)
print(D['school'])
D['city'] = 'Baltimore'
print(D)

D['buildings'] = ['Bloomberg', 'Malone', 'Latrobe']
print(D)
print(D['buildings'][2])
build = 'buildings'
print(D[build])

E = {1: 'hi'}
print(E[1])

# files
f = open('script-0.py', 'r')
text = f.read() #reads the whole file at once
print(text)
f.seek(0) # resets the pointer to the beginning
for line in f: # reads line by line
    print(line.split())

# completely remove this variable
del H
#print(H)
