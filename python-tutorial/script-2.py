#!/usr/bin/env python

from input_functions import get_input, check_length

################
# begin script #
################

# get user input
user_in = get_input()

# repeat until a 'q' appears
while user_in.find('q') == -1:
    # determine whether this is a single character or a string
    check_length(user_in)

    # print each character
    for i in range(len(user_in)):
        print('    user_in[%d] = %s' % (i, user_in[i]))

    for char in user_in:
        print('    ' + char)

    # ask again
    user_in = get_input()
