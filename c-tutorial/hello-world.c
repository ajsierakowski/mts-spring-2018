#include <stdlib.h>
#include <stdio.h>

#include "readwrite.h"

int main(int argc, char *argv[]) {
  printf("Hello world!\n");

  // parse the command line arguments
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");

  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // read an input file
  float a = 0.;
  float b = 0.;
  float c = 0.;

  read("input", &a, &b, &c);

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  return EXIT_SUCCESS;
}
